import 'package:expenseapp/database/moordatabase.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class Suggested extends StatefulWidget {
  final List<IncomeData> incomeList;

  const Suggested({Key key, this.incomeList}) : super(key: key);
  @override
  _SuggestedState createState() => _SuggestedState();
}

class _SuggestedState extends State<Suggested> {
  //Variables
  Stream data;
  String selected = 'Books';
  final scaffoldKey = GlobalKey<ScaffoldState>();
  double currentBalance;
  parseDate(DateTime date) {
    return "${date.year}/${date.month}/${date.day}";
  }
  List<Categorie> categoriesList =[];
  List<Item> itemsList = [];
  List<Item> temp =[];

   _buildItemsList(BuildContext context) {
              return ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: temp.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return Neumorphic(
                    style: NeumorphicStyle(
                      shape: NeumorphicShape.flat,
                      depth: 3.0,
                      intensity: 1.0
                    ),
                    child: ListTile(
                      dense: true,
                      title:Text(temp[index].name),
                      subtitle: Text(temp[index].categoryname),
                      trailing: Text("Rs. "+temp[index].price.toString()),
                      leading: CircleAvatar(
                        radius: 22,
                        backgroundColor: Colors.blueGrey[200],
                        child: Image.asset(temp[index].categoryname=='Bikes'?'lib/assets/motorcycle.png':
                        temp[index].categoryname=='Clothes'?'lib/assets/clothing.png':
                        temp[index].categoryname=='Books'?'lib/assets/book.png':
                        'lib/assets/wireless-headphones.png',
                        fit: BoxFit.contain,height: 40,)
                      ))
                  );
                });
          
  }
  FutureBuilder<List<Categorie>> _buildCategory(BuildContext context){
    var db = Provider.of<AppDatabase>(context);
    return FutureBuilder(
      future: db.categoriesDao.getAllCategory(),
      builder: (context,snap){
        if(snap.hasData){
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: snap.data.map((e){
          return Container(
            padding: EdgeInsets.all(5.0),
          decoration: BoxDecoration(
            color: Colors.blueGrey[500],
            borderRadius: BorderRadius.circular(15.0)
          ),
          child: Center(child: Text(e.name,style: TextStyle(color: Colors.white),)));
        }).toList());
        }
        return CircularProgressIndicator();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    calculate();
    super.initState();
  }
  
  
  calculate()async{
     var db = Provider.of<AppDatabase>(context,listen: false);
   await db.sessionDao.getSessionId().then((value){
     value.forEach((element) { 
    db.balanceDao.getbalance(element.userId).listen((event) {
      if(mounted)
      setState(() {
        currentBalance = event.totalBalance;
      });
      print(currentBalance);
      db.itemDao.getAllItems(currentBalance).listen((event) { 
      if(event!=null)
          setState(() {
          temp = event;  
          });
      print("List: $itemsList");
    });
    });   
     });
      
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: Padding(
        padding: const EdgeInsets.only(top:10),
        child: Column(
          children: <Widget>[
            Center(
              child: Container(
                child: Text('Your Subscribed Categories',style: TextStyle(fontSize: 20),)),
            ),
            Divider(thickness: 3,),
            SizedBox(height: 5,),
            _buildCategory(context),
            Divider(),
          Expanded(child: _buildItemsList(context))],
        ),
      ),
    );
  }
}
