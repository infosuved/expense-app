import 'package:expenseapp/database/moordatabase.dart';
import 'package:expenseapp/pages/landing.dart';
import 'package:flutter/material.dart';
import 'package:flutter_holo_date_picker/flutter_holo_date_picker.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:moor_flutter/moor_flutter.dart' as moor;
import 'package:provider/provider.dart';

class OnBoarding extends StatefulWidget {
  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {

  final introkey = GlobalKey();
  bool books = false;
  bool bikes = false;
  bool clothes =false;
  bool gadgets = false;
  String userId;
  String name = "";
  bool showerror = false;
  List<String> category =[];
  DateTime dateTime = new DateTime.now();
  double addamount;
  bool addsucess = false;
  bool showError = false;
  TextEditingController dateController = new TextEditingController();
  final formKey = GlobalKey<FormState>();
  final scaffoldkey = GlobalKey<ScaffoldState>();
  FlutterSecureStorage secureStorage;

  Column sucessful(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Icon(Icons.check_circle,color: Colors.green,size: 50,),
        Text('Sucessfully Added')
      ],
    );
  
  }

  @override
  void initState() {
    final db = Provider.of<AppDatabase>(context,listen: false);
    secureStorage = new FlutterSecureStorage();
    db.sessionDao.getSessionId().then((value){ 
      print('UserSession: ${value.length}');
         setState(() {
      userId = value[0].userId;
    });
    print('User Id: $userId');
    } );
     
  //  db.userDao.getUser(userId).then((value){
  //    print('User');
  //        setState(() {
  //      name = value[0].fullname;
  //    });
     
  //  });
    super.initState();
  }
  @override
  void dispose() {
    formKey.currentState?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context){
    final db = Provider.of<AppDatabase>(context);
    return SafeArea(
      child: Scaffold(
        key: scaffoldkey,
        body: IntroductionScreen(
          
          onDone: ()async{
            print(category);
            if(category.length==0){
              print("error");
              setState(() {
                showerror = true;
              });
            }
            else{
              if(addamount==null){
                db.balanceDao.addBalance(BalanceCompanion(userId: moor.Value(userId),totalBalance: moor.Value(0)
                                ,balanceDate: moor.Value(DateTime.now())));
              }
              await db.categoriesDao.initialCategory(category);
              await db.itemDao.initialItems(userId);
              await secureStorage.write(key: userId, value: 'true')
            .whenComplete(() => Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (cont)=>Landing())));
            }
            
          },
          // showNextButton: addsucess,
          next: Text("Next"),
          key: introkey,
          pages: [
            PageViewModel(
              title: 'Welcome to Expense Tracker',
              body: name!=null?'$name':"User",
              image: Align(alignment: Alignment.bottomCenter,child: Image.asset('lib/assets/logo.png',scale: 2,),)
            ),
              PageViewModel(
              // title: 'Enter Your Initial Income',
              titleWidget: Align(alignment: Alignment.bottomCenter,child: SvgPicture.asset('lib/assets/wallet.svg',height: 150,)),
              bodyWidget: addsucess==false? Container(
                padding: EdgeInsets.only(left: 20,right: 20,top: 50),
                height: MediaQuery.of(context).size.height/3,
                child: Form(
                  key: formKey,
                  child: Column(
                    children: <Widget>[
                      Text('Enter Your Inital Income'),
                      TextFormField(
                        keyboardType: TextInputType.number,
                        onSaved: (val){
                          setState(() {
                            addamount = double.tryParse(val);
                          });
                          print('$addamount');
                        },
                        validator: (value){
                          var amount = double.tryParse(value);
                          if(value.isEmpty){
                            return 'Cannot be empty';
                          }else if(amount<500){
                            return 'Amount Cannot Be Less Than 500';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10.0),
                          border: OutlineInputBorder(),
                          isDense: true,
                          icon: Icon(SimpleLineIcons.wallet,color: Colors.green,),
                          hintText: 'Enter Amount',
                          labelText: 'Add Income'
                        ),
                      ),
                      SizedBox(height: 10,),
                      TextField(
                        controller: dateController,
                        readOnly: true,
                        decoration: InputDecoration(
                          hintText: 'Select Date',
                          contentPadding: EdgeInsets.all(10.0),
                          isDense: true,
                          border: OutlineInputBorder(),
                          icon: Icon(SimpleLineIcons.calendar,color: Colors.green,)
                        ),
                        onTap: ()async{
                          var datepicked = await DatePicker.showSimpleDatePicker(
                            context,
                            confirmText: 'Select',
                            cancelText: 'Cancel',
                            initialDate: DateTime(2019),
                            firstDate: DateTime(1960),
                            lastDate: DateTime(2021),
                            dateFormat: 'dd-MMMM-yyyy',
                            looping: true,
                            pickerMode: DateTimePickerMode.date
                          );
                          if(datepicked!=null){
                            setState(() {
                              dateTime = datepicked;
                            dateController.text = "${datepicked.year}/${datepicked.month}/${datepicked.day}";  
                            });
                            print(dateTime.day);
                          }
                        },
                      ),
                      SizedBox(height: 10,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          NeumorphicButton(
                            child: Text("Add"),
                            style: NeumorphicStyle(
                              shape: NeumorphicShape.flat,
                              depth: 3,
                              intensity: 1
                            ),
                            onClick: ()async{
                              if(formKey.currentState.validate()){
                                setState(() {
                                  formKey.currentState.save();
                                });
                              final income = IncomeCompanion(userId: moor.Value(userId),totalIncome: moor.Value(addamount),incomeDate:moor.Value(dateTime));
                              await db.incomeDao.addIncome(income).whenComplete((){
                                db.balanceDao.addBalance(BalanceCompanion(userId: moor.Value(userId),totalBalance: moor.Value(addamount)
                                ,balanceDate: moor.Value(DateTime.now())));
                                scaffoldkey.currentState.showSnackBar(new SnackBar(content: Text('Sucessfully Added'),duration: Duration(seconds: 2),onVisible: (){
                                  setState(() {
                                    addsucess = true;
                                  });
                                },));
                              }).catchError((error){
                                print(error);
                              });
                              }
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ):sucessful(),
            ),
             PageViewModel(
               footer: showerror==true?SizedBox(child: Text("Alert: Select At Least One!",style: TextStyle(

               fontWeight: FontWeight.w900,color: Colors.red))):showError==true? SizedBox(child: Text("Alert: Select At Least One!",style: TextStyle(

               fontWeight: FontWeight.w900,color: Colors.red))):Row(),
              title: 'Select At Least One Category',
              image: Align(alignment: Alignment.bottomCenter,child: SvgPicture.asset('lib/assets/noitems.svg',height: 200,),),
              bodyWidget: Card(
                elevation: 1.0,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      ChoiceChip(label: Text('Books'),selected: books,onSelected: (s){
                        if(s==true ){
                          category.add('Books');
                        }else{
                          if(category.contains('Books')){
                          category.remove('Books');
                        }
                        }
                        setState(() {
                          books =s;
                        });
                      },selectedColor: Colors.teal,labelStyle: TextStyle(color: books!=true?Colors.black:Colors.white,)),
                      ChoiceChip(label: Text('Bikes'),selected: bikes,onSelected: (s){
                        if(s==true ){
                          category.add('Bikes');
                        }else{
                          if(category.contains('Bikes')){
                          category.remove('Bikes');
                        }
                        }
                        setState(() {
                          bikes =s;
                        });
                      },selectedColor: Colors.teal,labelStyle: TextStyle(color: bikes!=true?Colors.black:Colors.white,)),
                      ChoiceChip(label: Text('Clothes'),selected: clothes,onSelected: (s){
                        if(s==true ){
                          category.add('Clothes');
                        }else{
                          if(category.contains('Clothes')){
                          category.remove('Clothes');
                        }
                        }
                        setState(() {
                          clothes =s;
                        });
                      },selectedColor: Colors.teal,labelStyle: TextStyle(color: clothes!=true?Colors.black:Colors.white,)),
                      ChoiceChip(label: Text('Electronics'),selected: gadgets,onSelected: (s){
                        if(s==true ){
                          category.add('Electronics');
                        }else{
                          if(category.contains('Electronics')){
                          category.remove('Electronics');
                        }
                        }
                        setState(() {
                          gadgets =s;
                        });
                      },selectedColor: Colors.teal,labelStyle: TextStyle(color: gadgets!=true?Colors.black:Colors.white,)),
                      
                    ],
                  ),
                ),
              )
            ),
          
          ],
          initialPage: 0, 
          done: Text('Finish'),
        ),
      ),
    );
  }
}