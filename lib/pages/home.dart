import 'package:expenseapp/database/moordatabase.dart';
import 'package:flutter/material.dart';
import 'package:flutter_holo_date_picker/date_time_formatter.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin{

  Map<String, double> data ={};
  bool toggle =true;
  List<IncomeData> inc =[];
  List<ExpenseData> exp =[];
  double balance = 0;
  List<double> bal =[];
  String name;
  String phone;
  double totalIncome = 0;
  String userId;
  TabController tabController;
  DateTimeFormatter dateTimeFormatter = new DateTimeFormatter();
  
  
  @override
  void initState() {
    super.initState();
    tabController = new TabController(length: 2, vsync: this);
    final db = Provider.of<AppDatabase>(context,listen: false);
    db.sessionDao.getSessionId().then((val){
      if(val!=null && val.length==1){
          setState(() {
          userId = val.single.userId;
        });
        
      db.incomeDao.getIncomeList(userId).listen((value){ 
        print('Income: $value');
        setState(() {
       inc = value;   
        });
       
      // inc.forEach((element) { 
      //   bal.add(element.totalIncome);
      // });
     }
     );
     db.expenseDao.getExpenseList(userId).listen((value){
       print('Expense: $value');
       setState(() {
       exp = value;  
       });
       
     });
     db.balanceDao.getbalance(userId).listen((event) { 
       if(event!=null){
         if(mounted)
       setState(() {
       balance = event.totalBalance;  
       });
       }
       print('Current Balance: $balance');
     });
     db.userDao.getUser(userId).then((value){
       print(value);
       value.forEach((element) { 
         setState(() {
       name = element.fullname;
       phone = element.phone;  
       });
       });
       
     });
     }
    });
    // data.putIfAbsent("Income", ()=>income);
    // data.putIfAbsent("Expenses", ()=>expenses);
    // data.putIfAbsent("Balance", ()=>balance);
    
  }

  @override
  void dispose() {
    tabController?.dispose();
    super.dispose();
  }

  parseDate(DateTime date){
    return Text('${date.year}-${date.month}-${date.day}');
  }

  parseTime(DateTime time){
    time = time.toLocal();
    return Text('${time.hour}:${time.minute} ${time.hour<12?"AM":"PM"}');
  }


  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context);
      final db = Provider.of<AppDatabase>(context,listen: false);
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: SingleChildScrollView(
        padding: EdgeInsets.all(15),
          child: Column(
            children: <Widget>[
              Neumorphic(
                boxShape: NeumorphicBoxShape.roundRect(borderRadius: BorderRadius.circular(10.0)),
                style: NeumorphicStyle(
                  shape: NeumorphicShape.flat,
                  depth: 3,
                  lightSource: LightSource.top,
                  intensity: 1.0,
                  color: Colors.white
                ),
                child: Container(
                  padding: EdgeInsets.all(5.0),
                  height: 100,
                  width: media.size.width,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      parseDate(DateTime.now()),
                      Divider(color: Colors.blueGrey,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          CircleAvatar(
                                child: Icon(SimpleLineIcons.user),
                              ),
                          Column(
                            children: <Widget>[
                              Text(name!=null?name:'User'),
                              Text(phone!=null?phone:'N/A')
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Text("Balance",style: TextStyle(color: Colors.black38),),
                              Text("$balance")
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Text("Recent Activites",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
              SizedBox(height: 10,),
              Neumorphic(
                padding: EdgeInsets.all(10.0),
                style: NeumorphicStyle(
                  color: Colors.white,
                  depth: 3,
                  intensity: 1,
                  shape: NeumorphicShape.flat,
                  lightSource: LightSource.topRight
                ),
                boxShape: NeumorphicBoxShape.roundRect(borderRadius: BorderRadius.circular(10.0)),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                     TabBar(
                       controller: tabController,
                       tabs: [
                       OutlineButton(child: Text("Income"),onPressed: (){
                         tabController.animateTo(0);
                       },color: Colors.blueGrey,textColor: Colors.blueGrey),
                       OutlineButton(child: Text("Expense"),onPressed: (){
                         tabController.animateTo(1);
                       },color: Colors.blueGrey,textColor: Colors.blueGrey)
                     ]),
                    Container(
                       height: media.size.height/2,
                       width: media.size.width,
                       padding: EdgeInsets.all(10),
                       child: LiquidPullToRefresh(
                         height: 50,
                         springAnimationDurationInMilliseconds: 200,
                         onRefresh: () async{
                           if(userId==null){
                            return null;
                           }
                           await db.expenseDao.getAllExpense(userId).then((value){
                             setState(() {
                               exp = value;
                             });
                           });
                           return db.incomeDao.getAllIncome(userId).then((value){
                             setState(() {
                               inc = value;
                             });
                           });
                         },backgroundColor: Colors.white,
                         child: TabBarView(
                           controller: tabController,
                           children: [
                             ListView.builder(
                               itemCount: inc.length,
                               shrinkWrap: true,
                               itemBuilder: (context,index){
                               return inc.length>0 ?Column(
                                 children: <Widget>[
                                   ListTile(title:Text('Rs '+ inc[index].totalIncome.toString()),leading: Icon(SimpleLineIcons.wallet,size: 20,color: Colors.green,),
                                   trailing: parseDate(inc[index].incomeDate),
                                   dense: true,),
                                   Divider(height: 0,)
                                 ],
                               ):Center(child: Text("No Data Recorded"),);
                             }),
                             ListView.builder(
                               itemCount: exp.length,
                               shrinkWrap: true,
                               itemBuilder: (context,index){
                               return exp.length>0? Column(
                                 children: <Widget>[
                                   ListTile(title:Text('Rs. '+ exp[index].totalExpense.toString()),
                                    subtitle: Text(exp[index].description),
                                   leading: Icon(FontAwesome.money,color: Colors.red,),
                                   trailing: parseDate(exp[index].expenseDate),),
                                 ],
                               ):Center(child: Text("No Data Recorded"),);
                             })
                         ]),
                       ),
                     )
                    ],
                  ),
                ),
              ),

            ],
          )
      ),
    );
  }
}