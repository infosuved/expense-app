import 'package:expenseapp/database/moordatabase.dart';
import 'package:expenseapp/pages/landing.dart';
import 'package:expenseapp/pages/onboarding.dart';
import 'package:expenseapp/pages/sign_in/register.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:first_time_screen/first_time_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  bool showPassword = true;
  String username="";
  String password="";
  bool login = false;
  String userId;
  bool phoneVerified = false;
  FirebaseUser firebaseUser;
  List<User> users =[];
  
  final scaffold = GlobalKey<ScaffoldState>();
  final formkey= GlobalKey<FormState>();
  FlutterSecureStorage secureStorage;

  @override
  void initState() {
    FirebaseAuth.instance.currentUser().then((value){
      setState(() {
        firebaseUser = value;
      });
    });
    secureStorage = new FlutterSecureStorage();
    // secureStorage.read(key: 'phoneVerified').then((value){
    //   if(value=='true'){
    //     setState(() {
    //       phoneVerified = true;
    //     });
    //   }
    // });
   
     final db = Provider.of<AppDatabase>(context,listen: false);
     db.userDao.getAllUsers().then((value){
       if(value.length!=0){
         print("List: $value");
       setState(() {
         users = value;
         userId = value.single.phone;
       });
       }
     
      secureStorage.read(key: userId).then((value){
       print(value);
      if(value=='true'){
        setState(() {
          login = true;
        });
      }
    });
    });
    super.initState();
  }

  showProgress(BuildContext context){
    return showDialog(context: context,barrierDismissible: false, builder: (context){
        return Center(child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircularProgressIndicator(backgroundColor: Colors.teal,),
            Text("Logging In..",style: TextStyle(color: Colors.white,fontSize: 13,decoration: TextDecoration.none),),
          ],
        ));
    });
  }

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    final db = Provider.of<AppDatabase>(context);
   
    return SafeArea(
      child: Scaffold(
        key: scaffold,
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Neumorphic(
                style: NeumorphicStyle(
                  shape: NeumorphicShape.concave,
                  intensity: 1.0,
                  depth: 2.0,
                  lightSource: LightSource.bottom,
                  color: Colors.teal
                ),
                child: Container(
                  padding: EdgeInsets.only(top: 50),
                  height: media.height,
                  width: media.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(100),bottomRight: Radius.circular(100)),
                    color: Colors.white,
                    image: DecorationImage(image:AssetImage('lib/assets/log.png'),
                    colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.1), BlendMode.dstATop),fit: BoxFit.cover)
                  ),
                  child: Form(
                    key: formkey,
                    child: 
                  Column(
                    children: <Widget>[
                      Text("Login",style: TextStyle(fontSize: 40)),
              SizedBox(height: 10,),
                      Container(
                        width: media.width/1.5,
                        child: TextFormField(
                          keyboardType: TextInputType.phone,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(10)
                          ],
                          validator: (value){
                            if(value.isEmpty){
                              return 'Enter Phone';
                            }
                            return null;
                          },
                          onSaved: (value){
                            setState(() {
                              username = value;
                            });
                          },
                          decoration: InputDecoration(
                            icon: Icon(SimpleLineIcons.screen_smartphone),
                            labelText: "Phone",
                            hintText: "Enter Registered Phone"),
                        ),
                        
                      ),
                      Container(
                        width: media.width/1.5,
                        child: TextFormField(
                          validator: (value){
                            if(value.isEmpty){
                            return 'Enter Password';
                            }return null;
                          } 
                          ,
                          onSaved: (value){
                            setState(() {
                              password= value;
                            });
                          },
                          obscureText: showPassword,
                          decoration: InputDecoration(
                             suffixIcon: IconButton(icon: Icon(showPassword? Entypo.eye:Entypo.eye_with_line), onPressed: (){
                              setState(() {
                                showPassword = !showPassword;
                              });
                            }),
                            icon: Icon(SimpleLineIcons.lock),
                            labelText: "Password",
                            hintText: "Enter Password"),
                        )),
                      
                        SizedBox(height: 20,),
                        NeumorphicButton(
                          style: NeumorphicStyle(
                            shape: NeumorphicShape.flat,
                            color: Colors.teal
                          ),
                          child: Text("Login",style: TextStyle(color: Colors.white)),
                          onClick: ()async{
                            if(formkey.currentState.validate()){
                              setState(() {
                              formkey.currentState.save();  
                              });
                            db.userDao.login(username,password).then((value){
                              print(value);
                              if(value==true){
                                print("Logged In");
                                showProgress(context);
                                if(login==true){
                                  Future.delayed(Duration(seconds: 2),()=> Navigator.push(context, MaterialPageRoute(builder: (con)=>Landing())));
                                }else if(login==false){
                                Future.delayed(Duration(seconds: 2),()=> Navigator.push(context, MaterialPageRoute(builder: (con)=>OnBoarding())));
                               }else if(phoneVerified==false){
                                print("Firebase Failed");
                              scaffold.currentState.showSnackBar(SnackBar(content: Text("User Not Verified"),
                              behavior: SnackBarBehavior.floating,backgroundColor: Colors.red,));
                              }
                              }
                              else{
                                print("failed");
                              scaffold.currentState.showSnackBar(SnackBar(content: Text("Your Username or Password do not match"),
                              behavior: SnackBarBehavior.floating,backgroundColor: Colors.red,));
                              }
                            });
                            }
                          },
                        ),
                        Divider(color: Colors.teal,height: 40),
                        Text("Not Registered Yet?",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)),
                        SizedBox(height: 20),
                        NeumorphicButton(
                          style: NeumorphicStyle(
                            shape: NeumorphicShape.flat,
                            color: Colors.teal
                          ),
                          child: Text("Register",style: TextStyle(color: Colors.white)),
                          onClick: (){
                            if(users.length>=1){
                              scaffold.currentState.showSnackBar(SnackBar(content: Text("Currently only supported for single user."),
                              behavior: SnackBarBehavior.floating,backgroundColor: Colors.red,));
                            }else{
                              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>Register()));
                            }
                          },
                        )
                    ],
                  )),
                ),
              )
            ],
          ),
        ),
        
      ),
    );
  }
}