import 'package:expenseapp/database/moordatabase.dart';
import 'package:expenseapp/pages/sign_in/login.dart';
import 'package:expenseapp/pages/verify.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moor_flutter/moor_flutter.dart' as moor;
import 'package:provider/provider.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  bool showPassword = true;
  bool showConfirm = true;
  final formkey = GlobalKey<FormState>();
  final scaffold = GlobalKey<ScaffoldState>();
  TextEditingController username = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController phone = new TextEditingController();
  List<User> users =[];

  @override
  void dispose() {
    username?.dispose();
    password?.dispose();
    phone?.dispose();
    super.dispose();
  }
  
  @override
  void initState() {
    super.initState();
  }


  showProgress(BuildContext context){
    return showDialog(context: context,barrierDismissible: false, builder: (context){
        return Center(child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircularProgressIndicator(backgroundColor: Colors.teal,),
            Text("Checking Database..",style: TextStyle(color: Colors.white,fontSize: 13,decoration: TextDecoration.none),),
          ],
        ));
    });
  }
  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    final appDatabase =Provider.of<AppDatabase>(context);
    return SafeArea(
      child: Scaffold(
        key: scaffold,
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Neumorphic(
                style: NeumorphicStyle(
                  shape: NeumorphicShape.concave,
                  intensity: 1.0,
                  depth: 2.0,
                  lightSource: LightSource.bottom,
                  color: Colors.teal
                ),
                child: Container(
                  padding: EdgeInsets.only(top: 50),
                  height: media.height,
                  width: media.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(100),bottomRight: Radius.circular(100)),
                    color: Colors.white,
                    image: DecorationImage(image:AssetImage('lib/assets/log.png'),
                    colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.1), BlendMode.dstATop),fit: BoxFit.cover)
                  ),
                  child: Form(
                    key: formkey,
                    child: 
                  Column(
                    children: <Widget>[
                      Text("Register",style: TextStyle(fontSize: 40)),
              SizedBox(height: 10,),
                      Container(
                        width: media.width/1.5,
                        child: TextFormField(
                          keyboardType: TextInputType.phone,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(10)
                          ],
                          controller: phone,
                          validator: (data){
                            if(data.isEmpty){
                              return 'Phone Cannot Be Empty';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            icon: Icon(SimpleLineIcons.user),
                            labelText: "Phone",
                            hintText: "Enter Valid Phone Number"),
                        ),
                        
                      ),
                      Container(
                        width: media.width/1.5,
                        child: TextFormField(
                          controller: username,
                          validator: (data){
                            if(data.isEmpty){
                              return 'Full Name Cannot Be Empty';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            icon: Icon(SimpleLineIcons.user),
                            labelText: "Full Name",
                            hintText: "Enter Your Full Name"),
                        ),
                        
                      ),
                      Container(
                        width: media.width/1.5,
                        child: TextFormField(
                          controller: password,
                          validator: (data){
                            if(data.isEmpty){
                              return 'Password Cannot Be Empty';
                            }return null;
                          },
                          obscureText: showPassword,
                          decoration: InputDecoration(
                             suffixIcon: IconButton(icon: Icon(showPassword? Entypo.eye:Entypo.eye_with_line), onPressed: (){
                              setState(() {
                                showPassword = !showPassword;
                              });
                            }),
                            icon: Icon(SimpleLineIcons.lock),
                            labelText: "Password",
                            hintText: "Enter Password"),
                        )),
                      
                      Container(
                        width: media.width/1.5,
                        child: TextFormField(
                          validator: (data){
                            if(data.isEmpty){
                              return 'Password Cannot Be Empty';
                            }else if(data!=password.text){
                              return "Your Password doesn't match with above password";
                            }return null;
                          },
                          obscureText: showConfirm,
                          decoration: InputDecoration(
                            icon: Icon(SimpleLineIcons.lock),
                            suffixIcon: IconButton(icon: Icon(showConfirm? Entypo.eye:Entypo.eye_with_line), onPressed: (){
                              setState(() {
                                showConfirm = !showConfirm;
                              });
                            }),
                            labelText: "Confirm Password",
                            hintText: "Repeat Password"),
                        )),
                        SizedBox(height: 20,),
                        NeumorphicButton(
                          style: NeumorphicStyle(
                            shape: NeumorphicShape.flat,
                            color: Colors.teal
                          ),
                          child: Text("Register",style: TextStyle(color: Colors.white)),
                          onClick: ()async{  //Logic for User Registration
                            if(formkey.currentState.validate()){
                              formkey.currentState.save();
                              appDatabase.userDao.checkUser(phone.text).then((check){
                                if(check==false){
                                  final adduser = UsersCompanion(fullname: moor.Value(username.text),password: moor.Value(password.text),phone: moor.Value(phone.text));
                                appDatabase.userDao.addUser(adduser).whenComplete((){
                                  showProgress(context);
                                  Future.delayed(Duration(seconds: 1),()=> scaffold.currentState.showSnackBar(SnackBar(content: Text("User Added Sucessfully",style: TextStyle(color: Colors.white),
                                ),backgroundColor: Colors.green,duration: Duration(seconds: 1),)));
                                Future.delayed(Duration(seconds: 1),()=>Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>Verify(phone: phone.text,))));
                              });
                                }else{
                                scaffold.currentState.showSnackBar(SnackBar(content: Text("Couldn't add User, phone no. already exists!",style: TextStyle(color: Colors.white),
                                ),backgroundColor: Colors.red,duration: Duration(seconds: 3)));
                                }
                              });
                              
                            }
                          },
                        ),
                        Divider(color: Colors.teal,height: 40),
                        Text("Already Have an Account?",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)),
                        SizedBox(height: 20),
                        NeumorphicButton(
                          style: NeumorphicStyle(
                            shape: NeumorphicShape.flat,
                            color: Colors.teal
                          ),
                          child: Text("Login",style: TextStyle(color: Colors.white)),
                          onClick: (){
                            Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>Login()));
                          },
                        )
                    ],
                  )),
                ),
              )
            ],
          ),
        ),
        
      ),
    );
  }
}