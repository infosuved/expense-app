import 'package:expenseapp/database/moordatabase.dart';
import 'package:expenseapp/pages/services/authservice.dart';
import 'package:expenseapp/pages/sign_in/login.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';


class Verify extends StatefulWidget {
  final phone;

  const Verify({Key key, this.phone}) : super(key: key);
  @override
  _VerifyState createState() => _VerifyState();
}

class _VerifyState extends State<Verify> {

  @override
  void initState() {
    secureStorage = new FlutterSecureStorage();
    verifyPhone('+977'+widget.phone);
    super.initState();
  }
  String phoneNo, verificationId, smsCode;
   bool codeSent = false;
   AuthCredential authCredential;
   bool loading = false;
   bool userverified = false;
   FlutterSecureStorage secureStorage;
   final scaffoldkey = GlobalKey<ScaffoldState>();
   final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        return null;
      },
      child: Scaffold(
        key: scaffoldkey,
        body: Container(
          decoration:BoxDecoration(
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(100),bottomRight: Radius.circular(100)),
                        color: Colors.white,
                        image: DecorationImage(image:AssetImage('lib/assets/log.png'),
                        colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.1), BlendMode.dstATop),fit: BoxFit.cover)
                      ),
          child: userverified==true?Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(Entypo.check,color: Colors.green,size: 30,),
              SizedBox(height: 20,),
              Text('User Verified Sucessfully!'),
              
            ],
          ):
           Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Verify Your Mobile Number',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 25),),
              Form(
                key: formKey,
                child: Container(
                  padding: EdgeInsets.all(10.0),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: 'Enter OTP Code',
                      hintText: 'Enter OTP Code Received on Mobile'
                    ),
                    validator: (value) {
                      if(value.isEmpty){
                        return 'Cannot Be Empty';
                      }
                      return null;
                    },
                    onChanged: (val){
                        setState(() {
                          this.smsCode = val;
                        });
                    },
                  ),
                ),
              ),
              FlatButton(onPressed: (){
                if(formKey.currentState.validate()){
                setState(() {
                  loading =true;
                });
                AuthService().signinWithOTP(smsCode,verificationId,context).then((value)async{
                  await secureStorage.write(key: 'phoneVerified', value: '$value').whenComplete((){
                    if(value==true){
                      setState(() {
                        userverified = true;
                      });
                      Future.delayed(Duration(seconds: 2),
                      ()=>Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Login())));
                    }else{
                      scaffoldkey.currentState.showSnackBar(SnackBar(content: Text('OTP Verification not Sucessful!')
                      ,backgroundColor: Colors.red,behavior: SnackBarBehavior.floating,));
                    }
                  });
                  print(value);
                });
                }
              }, child: loading==false? Text('Verify'):CircularProgressIndicator(),color: Colors.teal,textColor: Colors.white,),
               FlatButton(onPressed: ()async{
                await verifyPhone('+977'+widget.phone);
              }, child: Text('Resend'),color: Colors.teal,textColor: Colors.white,)
            ],
          ),
        ),
      ),
    );
  }

  Future<void> verifyPhone(phoneNo) async {
   final PhoneVerificationCompleted verified = (AuthCredential user)async{
     print('${user.providerId}');
     setState(() {
       this.authCredential = user;
     });
    // await FirebaseAuth.instance.signInWithCredential(authCredential).then((value){
    //   if(value.user!=null){

    //   }
    // });

   };

    final PhoneVerificationFailed verificationfailed =
        (AuthException authException) {
      print('Failed: ${authException.message}');
    };

    final PhoneCodeSent smsSent = (String verId, [int forceResend]) {
      setState(() {
        this.verificationId = verId;
        this.codeSent = true;
      });
    };

    final PhoneCodeAutoRetrievalTimeout autoTimeout = (String verId) {
      this.verificationId = verId;
    };

    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phoneNo,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verified,
        verificationFailed: verificationfailed,
        codeSent: smsSent,
        codeAutoRetrievalTimeout: autoTimeout);
  }
}