import 'package:animated_splash/animated_splash.dart';
import 'package:expenseapp/database/moordatabase.dart';
import 'package:expenseapp/pages/landing.dart';
import 'package:expenseapp/pages/services/authservice.dart';
import 'package:expenseapp/pages/sign_in/login.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Checker extends StatefulWidget {
  @override
  _CheckerState createState() => _CheckerState();
}

class _CheckerState extends State<Checker> {

  UserSessionData sessionData;
  @override
  void initState() {
    getSession();
    super.initState();
  }
  getSession()async{
final db = Provider.of<AppDatabase>(context,listen: false);
 db.sessionDao.getSessionId().then((value){
   if(mounted && value!=null)
   value.forEach((element) { 
     setState(() {
     sessionData = element;
   });
   });
   
 });

  }


  @override
  Widget build(BuildContext context) {

    
    return AnimatedSplash(
        imagePath: 'lib/assets/logo.png',
        
      home:sessionData!=null?Landing():Login(),
      
      type: AnimatedSplashType.StaticDuration,
      duration: 3000,);
  }
}