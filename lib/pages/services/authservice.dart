import 'package:expenseapp/pages/onboarding.dart';
import 'package:expenseapp/pages/sign_in/login.dart';
import 'package:expenseapp/pages/verify.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AuthService {
  //Handles Auth
  handleAuth() {
    return StreamBuilder(
        stream: FirebaseAuth.instance.onAuthStateChanged,
        builder: (BuildContext context, snapshot) {
          if (snapshot.hasData) {
            return Login();
          } else {
            return Verify();
          }
        });
  }


Future<bool >signinWithOTP(sms,verId,BuildContext context)async{
  final AuthCredential authCredential = PhoneAuthProvider.getCredential(verificationId: verId, smsCode: sms);
  final   user = await FirebaseAuth.instance.signInWithCredential(authCredential);
  if(user.user.uid==null){
    return false;
  }
  return true;
}
  
}