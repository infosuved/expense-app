import 'package:expenseapp/database/moordatabase.dart';
import 'package:flutter/material.dart';
import 'package:flutter_holo_date_picker/date_time_formatter.dart';
import 'package:flutter_holo_date_picker/flutter_holo_date_picker.dart';
import 'package:provider/provider.dart';
import 'package:charts_flutter/flutter.dart'as charts;

class Stats extends StatefulWidget {
  @override
  _StatsState createState() => _StatsState();
}


class _StatsState extends State<Stats> {

  Map<String,double> inc ={};
  Map<String, double> exp ={};
  String user;
  List<IncomeData> income =[];
  List<ExpenseData> expense = [];
  bool loaded = false;
  DateTime dateTime = DateTime.now();

  parseWeek(DateTime dateTime){
    var date = DateTimeFormatter.formatDate(dateTime, 'MMMM d', DateTimePickerLocale.en_us);
    return date;
    
  }

  @override
  void initState() {
    if(mounted)
    calculate();
    super.initState();
  }

  calculate()async{
  final db = Provider.of<AppDatabase>(context,listen: false);
    await db.sessionDao.getSessionId().then((value){
      if(value!=null && value.length==1){
    setState(() {
       user = value.single.userId;
     });
      }
        
    });
    
    db.incomeDao.getIncomeList(user).listen((value){
     print(value);
      setState(() {
        income = value;
      });
    });
    //  db.expenseDao.orderbydate(user, dateTime).then((event) { 
    //    setState(() {
    //      expense = event;
    //      loaded = true;
    //    });
    // });
     db.expenseDao.getExpenseList(user).listen((value){
      setState(() {
        expense = value;
        loaded = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context);
    return Column(
      children: <Widget>[
      loaded==false?Center(child: CircularProgressIndicator(),) : Container(
          padding: EdgeInsets.all(10.0),
          height: media.size.height/1.8,
          width: media.size.width,
          child: new charts.BarChart(
            [
              new charts.Series<IncomeData,String>(
                colorFn: (_,__) => charts.MaterialPalette.teal.shadeDefault,
                id: 'Income',
                seriesCategory: 'Income',
                 data: income, 
              domainFn: (IncomeData data,_)=>parseWeek(data.incomeDate), 
              measureFn: (IncomeData data,_)=>data.totalIncome,
               outsideLabelStyleAccessorFn: (_,int data)=>charts.TextStyleSpec(fontSize: 8),
              labelAccessorFn: (IncomeData data,_)=>'Rs. ${data.totalIncome.toString()}'),

              new charts.Series<ExpenseData,String>(
                id: 'Expense', 
                seriesCategory: 'Expense',
                data: expense, 
              colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
              domainFn: (ExpenseData data,_)=>parseWeek(data.expenseDate), 
              measureFn: (ExpenseData data,_)=>data.totalExpense,
              outsideLabelStyleAccessorFn: (_,int data)=>charts.TextStyleSpec(fontSize: 8),
              labelAccessorFn: (ExpenseData data,_)=>'Rs. ${data.totalExpense}'),
            ],
            
            barGroupingType: charts.BarGroupingType.groupedStacked,
            barRendererDecorator: charts.BarLabelDecorator<String>(labelPosition: charts.BarLabelPosition.auto,
            ),
            // defaultRenderer: charts.BarRendererConfig(cornerStrategy: const charts.ConstCornerStrategy(30)),
            behaviors: [charts.SeriesLegend(showMeasures: true,horizontalFirst: false,entryTextStyle: charts.TextStyleSpec(fontSize: 18),
            legendDefaultMeasure: charts.LegendDefaultMeasure.sum,
            measureFormatter: (data)=>'Rs. $data',position: charts.BehaviorPosition.bottom,)],
          ),
        ),



      ],
    );
  }
}