import 'package:expenseapp/database/moordatabase.dart';
import 'package:expenseapp/pages/home.dart';
import 'package:expenseapp/pages/sign_in/login.dart';
import 'package:expenseapp/pages/stats.dart';
import 'package:expenseapp/pages/suggested.dart';
import 'package:expenseapp/widgets/adddialouge.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_holo_date_picker/date_picker.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:cuberto_bottom_bar/cuberto_bottom_bar.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:moor_flutter/moor_flutter.dart' as moor;
import 'package:provider/provider.dart';

class Landing extends StatefulWidget {
  @override
  _LandingState createState() => _LandingState();
}

class _LandingState extends State<Landing> with SingleTickerProviderStateMixin{

  TabController tabController;
  TextEditingController dateController = new TextEditingController();
  TextEditingController dController = new TextEditingController();
  TextEditingController priceController = new TextEditingController();
  DateTime dateTime ;
  DateTime expenseDate ;

  int currentindex =0;
  String userId;
  final formkey = GlobalKey<FormState>();
  final scaffoldkey = GlobalKey<ScaffoldState>();
  //for inserting values
  double addamount=0;
  double currentBalance=0;
  double amount;
  List<IncomeData> incomeList=[];
  //For Items
  double price;
  bool loading = false;
  String description;

  //Show Dialouge
  showIncomeDialouge(BuildContext context)async{
    final database = Provider.of<AppDatabase>(context,listen: false);
    await showDialog(context: context,
    barrierDismissible: false,
    builder: (context){
      return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10)
        ),
        child: Neumorphic(
          boxShape: NeumorphicBoxShape.roundRect(borderRadius: BorderRadius.circular(10)),
          style: NeumorphicStyle(
            shape: NeumorphicShape.flat,

          ),
          child: Container(
            padding: EdgeInsets.all(20),
            height: MediaQuery.of(context).size.height/3,
            child: Form(
              key: formkey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text("Add Income",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                  Divider(color: Colors.blueGrey,),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    onSaved: (val){
                      setState(() {
                        addamount = double.tryParse(val);
                      });
                      print('$addamount');
                    },
                    validator: (value){
                      var amount = double.tryParse(value);
                      if(value.isEmpty){
                        return 'Cannot be empty';
                      }else if(amount<=0){
                        return 'Amount cannot be negative or zero!';
                      }
                      
                      return null;
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(10.0),
                      border: OutlineInputBorder(),
                      isDense: true,
                      icon: Icon(SimpleLineIcons.wallet,color: Colors.green,),
                      hintText: 'Enter Amount',
                      labelText: 'Add Income'
                    ),
                  ),
                  
                  TextField(
                    controller: dateController,
                    readOnly: true,
                    decoration: InputDecoration(
                      hintText: 'Select Date',
                      contentPadding: EdgeInsets.all(10.0),
                      isDense: true,
                      border: OutlineInputBorder(),
                      icon: Icon(SimpleLineIcons.calendar,color: Colors.green,)
                    ),
                    onTap: ()async{
                      DateTime datepicked = await showDatePicker(
                        confirmText: 'Select',
                        cancelText: 'Cancel',
                        initialDate: dateTime,
                        firstDate: DateTime(DateTime.now().year-5),
                        lastDate: DateTime.now(), 
                        context: context,
                      );
                      if(datepicked!=null){
                        setState(() {
                          dateTime = datepicked;
                        dateController.text = "${datepicked.year}/${datepicked.month}/${datepicked.day}";  
                        });
                        
                      }
                    },
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      NeumorphicButton(
                        child: Text("Add"),
                        style: NeumorphicStyle(
                          shape: NeumorphicShape.flat,
                          depth: 3,
                          intensity: 1
                        ),
                        onClick: ()async{
                          if(formkey.currentState.validate()){
                            setState(() {
                              formkey.currentState.save();
                            });
                          final income = IncomeCompanion(userId: moor.Value(userId),totalIncome: moor.Value(addamount),incomeDate:moor.Value(dateTime));
                          await database.incomeDao.addIncome(income).whenComplete((){
                            // print('New Balance: ${addamount+currentBalance}');
                            final data = BalanceCompanion(userId: moor.Value(userId),
                            totalBalance: moor.Value(addamount+currentBalance),balanceDate:moor.Value(dateTime));
                            database.balanceDao.updateBalance(data);
                            scaffoldkey.currentState.showSnackBar(new SnackBar(content: Text('Sucessfully Added')));
                            priceController.clear();
                            Navigator.pop(context);
                            
                          });
                          }
                        },
                      ),
                  NeumorphicButton(
                        child: Text("Cancel"),
                        style: NeumorphicStyle(
                          shape: NeumorphicShape.flat,
                          depth: 3,
                          intensity: 1
                        ),
                        onClick: (){
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }

  showExpenseDialouge(BuildContext context)async{
    final database = Provider.of<AppDatabase>(context,listen: false);
    await showDialog(context: context,
    barrierDismissible: false,
    builder: (context){
      return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10)
        ),
        child: Neumorphic(
          boxShape: NeumorphicBoxShape.roundRect(borderRadius: BorderRadius.circular(10)),
          style: NeumorphicStyle(
            shape: NeumorphicShape.flat,

          ),
          child: Container(
            padding: EdgeInsets.all(20),
            height: MediaQuery.of(context).size.height/2.5,
            child: Form(
              key: formkey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text("Add Expense",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                  Divider(color: Colors.blueGrey),
                  TextFormField(
                    onSaved: (val){
                      setState(() {
                        description =val;
                      });
                      print(description);
                    },
                    validator: (value){
                      if(value.isEmpty){
                        return 'Cannot be empty';
                      }
                      
                      return null;
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(10.0),
                      border: OutlineInputBorder(),
                      isDense: true,
                      icon: Icon(FontAwesome5Solid.shopping_basket,color: Colors.blue,),
                      hintText: 'Enter Description',
                      labelText: 'Description'
                    ),
                  ),
                  
                  TextFormField(
                    controller: priceController,
                    keyboardType: TextInputType.number,
                    validator: (data){
                      var amount = double.parse(data); 
                      if(data.isEmpty){
                        return 'You forgot price';
                      }
                      else if(amount<=0){
                         return 'Amount cannot be negative or zero';
                      }
                      else if(currentBalance<amount){
                        return 'Insufficient Balance';
                      }
                      return null;
                    },
                    onSaved: (val){
                      setState(() {
                        price = double.tryParse(val);
                      });
                      print(price);
                    },
                    decoration: InputDecoration(
                      hintText: 'Enter Amount',
                      labelText: 'Price',
                      contentPadding: EdgeInsets.all(10.0),
                      isDense: true,
                      border: OutlineInputBorder(),
                      icon: Icon(MaterialCommunityIcons.cash_multiple,color: Colors.blue,)
                    ),
                  ),
                  TextField(
                    controller: dController,
                    readOnly: true,
                    decoration: InputDecoration(
                      hintText: 'Select Date',
                      contentPadding: EdgeInsets.all(10.0),
                      isDense: true,
                      border: OutlineInputBorder(),
                      icon: Icon(SimpleLineIcons.calendar,color: Colors.green,)
                    ),
                    onTap: ()async{
                      DateTime datepicked = await showDatePicker(
                        context: context,
                        confirmText: 'Select',
                        cancelText: 'Cancel',
                        initialDate: dateTime,
                        firstDate: DateTime(DateTime.now().year-5),
                        lastDate: DateTime.now()
                      );
                      if(datepicked!=null){
                        print(datepicked);
                        setState(() {
                          expenseDate = datepicked;
                        dController.text = "${datepicked.year}/${datepicked.month}/${datepicked.day}";  
                        });
                        
                      }
                    },
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      NeumorphicButton(
                        child: Text("Add"),
                        style: NeumorphicStyle(
                          shape: NeumorphicShape.flat,
                          depth: 3,
                          intensity: 1
                        ),
                        onClick: ()async{
                          if(formkey.currentState.validate()){
                            setState(() {
                              formkey.currentState.save();
                            });
                          final expense = ExpenseCompanion(totalExpense: moor.Value(price),userId: moor.Value(userId),
                          description: moor.Value(description), expenseDate: moor.Value(expenseDate) );
                          // final bal = BalanceData(userId: userId, totalBalance: currentBalance-price);
                          await database.expenseDao.addExpense(expense).whenComplete((){
                            final bal = BalanceCompanion(userId: moor.Value(userId),totalBalance: moor.Value(currentBalance-price),
                            balanceDate: moor.Value(expenseDate));
                            database.balanceDao.updateBalance(bal);
                            setState(() {
                            });
                            scaffoldkey.currentState.showSnackBar(new SnackBar(content: Text('Sucessfully Added'),));
                            priceController.clear();
                            Navigator.pop(context);
                          });
                          }
                        },
                      ),
                  NeumorphicButton(
                        child: Text("Cancel"),
                        style: NeumorphicStyle(
                          shape: NeumorphicShape.flat,
                          depth: 3,
                          intensity: 1
                        ),
                        onClick: (){
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }


  @override
  void initState() {
    tabController = new TabController(length: 3, vsync: this);
    final db = Provider.of<AppDatabase>(context,listen: false); 
    dateTime = DateTime.now();
    expenseDate = DateTime.now();
    db.sessionDao.getSessionId().then((val){
      val.forEach((element) { 
        setState(() {
        userId = element.userId;
      });
      });
      
      print("User Id: $userId");
    });
    db.balanceDao.getbalance(userId).listen((event) {
      if(event!=null){
      setState(() {
        currentBalance = event.totalBalance;
      });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    tabController?.dispose();
    dController?.dispose();
    priceController?.dispose();
    dateController?.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    final db = Provider.of<AppDatabase>(context); 
    return WillPopScope(
      onWillPop: (){
        return null;
      },
      child: DefaultTabController(
        length: 3,
        initialIndex: 0,
        child: Scaffold(
          key: scaffoldkey,
          floatingActionButton:SpeedDial(
            backgroundColor: Colors.blueGrey,
            curve: Curves.bounceIn,
            animatedIcon: AnimatedIcons.add_event,
            children: [
              SpeedDialChild(label: "Add Income",child: Icon(SimpleLineIcons.wallet),backgroundColor: Colors.green, onTap: (){
                showIncomeDialouge(context);
              }),
              SpeedDialChild(label: "Add Expenses",child: Icon(FontAwesome5.money_bill_alt),backgroundColor: Colors.red,onTap: (){
                showExpenseDialouge(context);
              }),
            ],
          ),
          bottomNavigationBar: CubertoBottomBar(
            selectedTab: currentindex,
            tabs: [
              TabData(iconData: SimpleLineIcons.home, title: "Home",onclick: (){
                tabController.animateTo(0);
              }),
              TabData(iconData: MaterialCommunityIcons.shopping, title: "Suggested",onclick: (){
                tabController.animateTo(1);
              }),
              TabData(iconData: SimpleLineIcons.pie_chart, title: "Stats",onclick: (){
                tabController.animateTo(2);
              }),
          ], onTabChangedListener:(index,str,col){
            setState(() {
              currentindex=index;
            });
            tabController.animateTo(currentindex);
          }),
          appBar: AppBar(
            actions: <Widget>[
              IconButton(icon: Icon(Icons.power_settings_new), onPressed: ()async{
                showDialog(context: context,barrierDismissible: false,
                builder: (context){
                  return Dialog(
                    child: Container(
                      height: 200,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Icon(MaterialCommunityIcons.logout),
                        Text('Do You Want to Logout?'),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                          loading==true?CircularProgressIndicator() :FlatButton(onPressed: ()async{
                            setState(() {
                     loading = true;
                   });
                               if(userId!=null){
                 await db.userDao.logout(userId).then((val){
                   if(val==true){
                       print("Logged out");
                       Future.delayed(Duration(seconds: 2),(){
                       Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>Login()));  
                       });
                   }else{
                       print("Failed");
                   }
                 }).catchError((error){
                   print("Error::$error");
                 });
                }
                            }, child: Text('Yes'),color: Colors.green,textColor: Colors.white,),
                            FlatButton(onPressed: (){
                              Navigator.pop(context);
                            }, child: Text('No'),color: Colors.red,textColor: Colors.white,)
                          ],
                        )
                      ],),
                    ),
                  );
                });
               
              })
            ],
            automaticallyImplyLeading: false,
            title:tabController.index==0?Text('Home') : tabController.index==1? Text("Suggested Items"):Text('Statistics'),
            centerTitle: true,
          ),
          body: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            controller: tabController,
            children: [
            Home(),
            Suggested(),
            Stats()
          ]),
          ),
      ),
    );
  }
}