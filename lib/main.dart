import 'package:expenseapp/database/moordatabase.dart';
import 'package:expenseapp/pages/checker.dart';
import 'package:expenseapp/pages/landing.dart';
import 'package:expenseapp/pages/sign_in/login.dart';
import 'package:flutter/material.dart';
import 'package:animated_splash/animated_splash.dart';
import 'package:provider/provider.dart';

void main(){
  runApp(
    Provider<AppDatabase>(create: (context)=>AppDatabase(),
    child: MyApp(),
    dispose: (context,db)=>db.close(),)
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(
      theme: ThemeData(primaryColor: Colors.blueGrey),
      home: Checker()
    );
  }
}
