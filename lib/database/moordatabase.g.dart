// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'moordatabase.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class ExpenseData extends DataClass implements Insertable<ExpenseData> {
  final int id;
  final double totalExpense;
  final String userId;
  final String description;
  final DateTime expenseDate;
  ExpenseData(
      {@required this.id,
      @required this.totalExpense,
      @required this.userId,
      this.description,
      this.expenseDate});
  factory ExpenseData.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final doubleType = db.typeSystem.forDartType<double>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return ExpenseData(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      totalExpense: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}total_expense']),
      userId:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}user_id']),
      description: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}description']),
      expenseDate: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}expense_date']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || totalExpense != null) {
      map['total_expense'] = Variable<double>(totalExpense);
    }
    if (!nullToAbsent || userId != null) {
      map['user_id'] = Variable<String>(userId);
    }
    if (!nullToAbsent || description != null) {
      map['description'] = Variable<String>(description);
    }
    if (!nullToAbsent || expenseDate != null) {
      map['expense_date'] = Variable<DateTime>(expenseDate);
    }
    return map;
  }

  factory ExpenseData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return ExpenseData(
      id: serializer.fromJson<int>(json['id']),
      totalExpense: serializer.fromJson<double>(json['totalExpense']),
      userId: serializer.fromJson<String>(json['userId']),
      description: serializer.fromJson<String>(json['description']),
      expenseDate: serializer.fromJson<DateTime>(json['expenseDate']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'totalExpense': serializer.toJson<double>(totalExpense),
      'userId': serializer.toJson<String>(userId),
      'description': serializer.toJson<String>(description),
      'expenseDate': serializer.toJson<DateTime>(expenseDate),
    };
  }

  ExpenseData copyWith(
          {int id,
          double totalExpense,
          String userId,
          String description,
          DateTime expenseDate}) =>
      ExpenseData(
        id: id ?? this.id,
        totalExpense: totalExpense ?? this.totalExpense,
        userId: userId ?? this.userId,
        description: description ?? this.description,
        expenseDate: expenseDate ?? this.expenseDate,
      );
  @override
  String toString() {
    return (StringBuffer('ExpenseData(')
          ..write('id: $id, ')
          ..write('totalExpense: $totalExpense, ')
          ..write('userId: $userId, ')
          ..write('description: $description, ')
          ..write('expenseDate: $expenseDate')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          totalExpense.hashCode,
          $mrjc(userId.hashCode,
              $mrjc(description.hashCode, expenseDate.hashCode)))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is ExpenseData &&
          other.id == this.id &&
          other.totalExpense == this.totalExpense &&
          other.userId == this.userId &&
          other.description == this.description &&
          other.expenseDate == this.expenseDate);
}

class ExpenseCompanion extends UpdateCompanion<ExpenseData> {
  final Value<int> id;
  final Value<double> totalExpense;
  final Value<String> userId;
  final Value<String> description;
  final Value<DateTime> expenseDate;
  const ExpenseCompanion({
    this.id = const Value.absent(),
    this.totalExpense = const Value.absent(),
    this.userId = const Value.absent(),
    this.description = const Value.absent(),
    this.expenseDate = const Value.absent(),
  });
  ExpenseCompanion.insert({
    this.id = const Value.absent(),
    @required double totalExpense,
    @required String userId,
    this.description = const Value.absent(),
    this.expenseDate = const Value.absent(),
  })  : totalExpense = Value(totalExpense),
        userId = Value(userId);
  static Insertable<ExpenseData> custom({
    Expression<int> id,
    Expression<double> totalExpense,
    Expression<String> userId,
    Expression<String> description,
    Expression<DateTime> expenseDate,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (totalExpense != null) 'total_expense': totalExpense,
      if (userId != null) 'user_id': userId,
      if (description != null) 'description': description,
      if (expenseDate != null) 'expense_date': expenseDate,
    });
  }

  ExpenseCompanion copyWith(
      {Value<int> id,
      Value<double> totalExpense,
      Value<String> userId,
      Value<String> description,
      Value<DateTime> expenseDate}) {
    return ExpenseCompanion(
      id: id ?? this.id,
      totalExpense: totalExpense ?? this.totalExpense,
      userId: userId ?? this.userId,
      description: description ?? this.description,
      expenseDate: expenseDate ?? this.expenseDate,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (totalExpense.present) {
      map['total_expense'] = Variable<double>(totalExpense.value);
    }
    if (userId.present) {
      map['user_id'] = Variable<String>(userId.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (expenseDate.present) {
      map['expense_date'] = Variable<DateTime>(expenseDate.value);
    }
    return map;
  }
}

class $ExpenseTable extends Expense with TableInfo<$ExpenseTable, ExpenseData> {
  final GeneratedDatabase _db;
  final String _alias;
  $ExpenseTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _totalExpenseMeta =
      const VerificationMeta('totalExpense');
  GeneratedRealColumn _totalExpense;
  @override
  GeneratedRealColumn get totalExpense =>
      _totalExpense ??= _constructTotalExpense();
  GeneratedRealColumn _constructTotalExpense() {
    return GeneratedRealColumn(
      'total_expense',
      $tableName,
      false,
    );
  }

  final VerificationMeta _userIdMeta = const VerificationMeta('userId');
  GeneratedTextColumn _userId;
  @override
  GeneratedTextColumn get userId => _userId ??= _constructUserId();
  GeneratedTextColumn _constructUserId() {
    return GeneratedTextColumn(
      'user_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  GeneratedTextColumn _description;
  @override
  GeneratedTextColumn get description =>
      _description ??= _constructDescription();
  GeneratedTextColumn _constructDescription() {
    return GeneratedTextColumn(
      'description',
      $tableName,
      true,
    );
  }

  final VerificationMeta _expenseDateMeta =
      const VerificationMeta('expenseDate');
  GeneratedDateTimeColumn _expenseDate;
  @override
  GeneratedDateTimeColumn get expenseDate =>
      _expenseDate ??= _constructExpenseDate();
  GeneratedDateTimeColumn _constructExpenseDate() {
    return GeneratedDateTimeColumn(
      'expense_date',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, totalExpense, userId, description, expenseDate];
  @override
  $ExpenseTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'expense';
  @override
  final String actualTableName = 'expense';
  @override
  VerificationContext validateIntegrity(Insertable<ExpenseData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('total_expense')) {
      context.handle(
          _totalExpenseMeta,
          totalExpense.isAcceptableOrUnknown(
              data['total_expense'], _totalExpenseMeta));
    } else if (isInserting) {
      context.missing(_totalExpenseMeta);
    }
    if (data.containsKey('user_id')) {
      context.handle(_userIdMeta,
          userId.isAcceptableOrUnknown(data['user_id'], _userIdMeta));
    } else if (isInserting) {
      context.missing(_userIdMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description'], _descriptionMeta));
    }
    if (data.containsKey('expense_date')) {
      context.handle(
          _expenseDateMeta,
          expenseDate.isAcceptableOrUnknown(
              data['expense_date'], _expenseDateMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  ExpenseData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return ExpenseData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $ExpenseTable createAlias(String alias) {
    return $ExpenseTable(_db, alias);
  }
}

class IncomeData extends DataClass implements Insertable<IncomeData> {
  final int id;
  final String userId;
  final double totalIncome;
  final DateTime incomeDate;
  IncomeData(
      {@required this.id,
      @required this.userId,
      @required this.totalIncome,
      this.incomeDate});
  factory IncomeData.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final doubleType = db.typeSystem.forDartType<double>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return IncomeData(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      userId:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}user_id']),
      totalIncome: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}total_income']),
      incomeDate: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}income_date']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || userId != null) {
      map['user_id'] = Variable<String>(userId);
    }
    if (!nullToAbsent || totalIncome != null) {
      map['total_income'] = Variable<double>(totalIncome);
    }
    if (!nullToAbsent || incomeDate != null) {
      map['income_date'] = Variable<DateTime>(incomeDate);
    }
    return map;
  }

  factory IncomeData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return IncomeData(
      id: serializer.fromJson<int>(json['id']),
      userId: serializer.fromJson<String>(json['userId']),
      totalIncome: serializer.fromJson<double>(json['totalIncome']),
      incomeDate: serializer.fromJson<DateTime>(json['incomeDate']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'userId': serializer.toJson<String>(userId),
      'totalIncome': serializer.toJson<double>(totalIncome),
      'incomeDate': serializer.toJson<DateTime>(incomeDate),
    };
  }

  IncomeData copyWith(
          {int id, String userId, double totalIncome, DateTime incomeDate}) =>
      IncomeData(
        id: id ?? this.id,
        userId: userId ?? this.userId,
        totalIncome: totalIncome ?? this.totalIncome,
        incomeDate: incomeDate ?? this.incomeDate,
      );
  @override
  String toString() {
    return (StringBuffer('IncomeData(')
          ..write('id: $id, ')
          ..write('userId: $userId, ')
          ..write('totalIncome: $totalIncome, ')
          ..write('incomeDate: $incomeDate')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          userId.hashCode, $mrjc(totalIncome.hashCode, incomeDate.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is IncomeData &&
          other.id == this.id &&
          other.userId == this.userId &&
          other.totalIncome == this.totalIncome &&
          other.incomeDate == this.incomeDate);
}

class IncomeCompanion extends UpdateCompanion<IncomeData> {
  final Value<int> id;
  final Value<String> userId;
  final Value<double> totalIncome;
  final Value<DateTime> incomeDate;
  const IncomeCompanion({
    this.id = const Value.absent(),
    this.userId = const Value.absent(),
    this.totalIncome = const Value.absent(),
    this.incomeDate = const Value.absent(),
  });
  IncomeCompanion.insert({
    this.id = const Value.absent(),
    @required String userId,
    @required double totalIncome,
    this.incomeDate = const Value.absent(),
  })  : userId = Value(userId),
        totalIncome = Value(totalIncome);
  static Insertable<IncomeData> custom({
    Expression<int> id,
    Expression<String> userId,
    Expression<double> totalIncome,
    Expression<DateTime> incomeDate,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (userId != null) 'user_id': userId,
      if (totalIncome != null) 'total_income': totalIncome,
      if (incomeDate != null) 'income_date': incomeDate,
    });
  }

  IncomeCompanion copyWith(
      {Value<int> id,
      Value<String> userId,
      Value<double> totalIncome,
      Value<DateTime> incomeDate}) {
    return IncomeCompanion(
      id: id ?? this.id,
      userId: userId ?? this.userId,
      totalIncome: totalIncome ?? this.totalIncome,
      incomeDate: incomeDate ?? this.incomeDate,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (userId.present) {
      map['user_id'] = Variable<String>(userId.value);
    }
    if (totalIncome.present) {
      map['total_income'] = Variable<double>(totalIncome.value);
    }
    if (incomeDate.present) {
      map['income_date'] = Variable<DateTime>(incomeDate.value);
    }
    return map;
  }
}

class $IncomeTable extends Income with TableInfo<$IncomeTable, IncomeData> {
  final GeneratedDatabase _db;
  final String _alias;
  $IncomeTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _userIdMeta = const VerificationMeta('userId');
  GeneratedTextColumn _userId;
  @override
  GeneratedTextColumn get userId => _userId ??= _constructUserId();
  GeneratedTextColumn _constructUserId() {
    return GeneratedTextColumn(
      'user_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _totalIncomeMeta =
      const VerificationMeta('totalIncome');
  GeneratedRealColumn _totalIncome;
  @override
  GeneratedRealColumn get totalIncome =>
      _totalIncome ??= _constructTotalIncome();
  GeneratedRealColumn _constructTotalIncome() {
    return GeneratedRealColumn(
      'total_income',
      $tableName,
      false,
    );
  }

  final VerificationMeta _incomeDateMeta = const VerificationMeta('incomeDate');
  GeneratedDateTimeColumn _incomeDate;
  @override
  GeneratedDateTimeColumn get incomeDate =>
      _incomeDate ??= _constructIncomeDate();
  GeneratedDateTimeColumn _constructIncomeDate() {
    return GeneratedDateTimeColumn(
      'income_date',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, userId, totalIncome, incomeDate];
  @override
  $IncomeTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'income';
  @override
  final String actualTableName = 'income';
  @override
  VerificationContext validateIntegrity(Insertable<IncomeData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('user_id')) {
      context.handle(_userIdMeta,
          userId.isAcceptableOrUnknown(data['user_id'], _userIdMeta));
    } else if (isInserting) {
      context.missing(_userIdMeta);
    }
    if (data.containsKey('total_income')) {
      context.handle(
          _totalIncomeMeta,
          totalIncome.isAcceptableOrUnknown(
              data['total_income'], _totalIncomeMeta));
    } else if (isInserting) {
      context.missing(_totalIncomeMeta);
    }
    if (data.containsKey('income_date')) {
      context.handle(
          _incomeDateMeta,
          incomeDate.isAcceptableOrUnknown(
              data['income_date'], _incomeDateMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  IncomeData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return IncomeData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $IncomeTable createAlias(String alias) {
    return $IncomeTable(_db, alias);
  }
}

class BalanceData extends DataClass implements Insertable<BalanceData> {
  final String userId;
  final double totalBalance;
  final DateTime balanceDate;
  BalanceData(
      {@required this.userId, @required this.totalBalance, this.balanceDate});
  factory BalanceData.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final doubleType = db.typeSystem.forDartType<double>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return BalanceData(
      userId:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}user_id']),
      totalBalance: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}total_balance']),
      balanceDate: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}balance_date']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || userId != null) {
      map['user_id'] = Variable<String>(userId);
    }
    if (!nullToAbsent || totalBalance != null) {
      map['total_balance'] = Variable<double>(totalBalance);
    }
    if (!nullToAbsent || balanceDate != null) {
      map['balance_date'] = Variable<DateTime>(balanceDate);
    }
    return map;
  }

  factory BalanceData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return BalanceData(
      userId: serializer.fromJson<String>(json['userId']),
      totalBalance: serializer.fromJson<double>(json['totalBalance']),
      balanceDate: serializer.fromJson<DateTime>(json['balanceDate']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'userId': serializer.toJson<String>(userId),
      'totalBalance': serializer.toJson<double>(totalBalance),
      'balanceDate': serializer.toJson<DateTime>(balanceDate),
    };
  }

  BalanceData copyWith(
          {String userId, double totalBalance, DateTime balanceDate}) =>
      BalanceData(
        userId: userId ?? this.userId,
        totalBalance: totalBalance ?? this.totalBalance,
        balanceDate: balanceDate ?? this.balanceDate,
      );
  @override
  String toString() {
    return (StringBuffer('BalanceData(')
          ..write('userId: $userId, ')
          ..write('totalBalance: $totalBalance, ')
          ..write('balanceDate: $balanceDate')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      userId.hashCode, $mrjc(totalBalance.hashCode, balanceDate.hashCode)));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is BalanceData &&
          other.userId == this.userId &&
          other.totalBalance == this.totalBalance &&
          other.balanceDate == this.balanceDate);
}

class BalanceCompanion extends UpdateCompanion<BalanceData> {
  final Value<String> userId;
  final Value<double> totalBalance;
  final Value<DateTime> balanceDate;
  const BalanceCompanion({
    this.userId = const Value.absent(),
    this.totalBalance = const Value.absent(),
    this.balanceDate = const Value.absent(),
  });
  BalanceCompanion.insert({
    @required String userId,
    @required double totalBalance,
    this.balanceDate = const Value.absent(),
  })  : userId = Value(userId),
        totalBalance = Value(totalBalance);
  static Insertable<BalanceData> custom({
    Expression<String> userId,
    Expression<double> totalBalance,
    Expression<DateTime> balanceDate,
  }) {
    return RawValuesInsertable({
      if (userId != null) 'user_id': userId,
      if (totalBalance != null) 'total_balance': totalBalance,
      if (balanceDate != null) 'balance_date': balanceDate,
    });
  }

  BalanceCompanion copyWith(
      {Value<String> userId,
      Value<double> totalBalance,
      Value<DateTime> balanceDate}) {
    return BalanceCompanion(
      userId: userId ?? this.userId,
      totalBalance: totalBalance ?? this.totalBalance,
      balanceDate: balanceDate ?? this.balanceDate,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (userId.present) {
      map['user_id'] = Variable<String>(userId.value);
    }
    if (totalBalance.present) {
      map['total_balance'] = Variable<double>(totalBalance.value);
    }
    if (balanceDate.present) {
      map['balance_date'] = Variable<DateTime>(balanceDate.value);
    }
    return map;
  }
}

class $BalanceTable extends Balance with TableInfo<$BalanceTable, BalanceData> {
  final GeneratedDatabase _db;
  final String _alias;
  $BalanceTable(this._db, [this._alias]);
  final VerificationMeta _userIdMeta = const VerificationMeta('userId');
  GeneratedTextColumn _userId;
  @override
  GeneratedTextColumn get userId => _userId ??= _constructUserId();
  GeneratedTextColumn _constructUserId() {
    return GeneratedTextColumn(
      'user_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _totalBalanceMeta =
      const VerificationMeta('totalBalance');
  GeneratedRealColumn _totalBalance;
  @override
  GeneratedRealColumn get totalBalance =>
      _totalBalance ??= _constructTotalBalance();
  GeneratedRealColumn _constructTotalBalance() {
    return GeneratedRealColumn(
      'total_balance',
      $tableName,
      false,
    );
  }

  final VerificationMeta _balanceDateMeta =
      const VerificationMeta('balanceDate');
  GeneratedDateTimeColumn _balanceDate;
  @override
  GeneratedDateTimeColumn get balanceDate =>
      _balanceDate ??= _constructBalanceDate();
  GeneratedDateTimeColumn _constructBalanceDate() {
    return GeneratedDateTimeColumn(
      'balance_date',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [userId, totalBalance, balanceDate];
  @override
  $BalanceTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'balance';
  @override
  final String actualTableName = 'balance';
  @override
  VerificationContext validateIntegrity(Insertable<BalanceData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('user_id')) {
      context.handle(_userIdMeta,
          userId.isAcceptableOrUnknown(data['user_id'], _userIdMeta));
    } else if (isInserting) {
      context.missing(_userIdMeta);
    }
    if (data.containsKey('total_balance')) {
      context.handle(
          _totalBalanceMeta,
          totalBalance.isAcceptableOrUnknown(
              data['total_balance'], _totalBalanceMeta));
    } else if (isInserting) {
      context.missing(_totalBalanceMeta);
    }
    if (data.containsKey('balance_date')) {
      context.handle(
          _balanceDateMeta,
          balanceDate.isAcceptableOrUnknown(
              data['balance_date'], _balanceDateMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {userId};
  @override
  BalanceData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return BalanceData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $BalanceTable createAlias(String alias) {
    return $BalanceTable(_db, alias);
  }
}

class Item extends DataClass implements Insertable<Item> {
  final int id;
  final String userId;
  final String categoryname;
  final String name;
  final double price;
  Item(
      {@required this.id,
      @required this.userId,
      this.categoryname,
      @required this.name,
      @required this.price});
  factory Item.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final doubleType = db.typeSystem.forDartType<double>();
    return Item(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      userId:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}user_id']),
      categoryname: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}categoryname']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      price:
          doubleType.mapFromDatabaseResponse(data['${effectivePrefix}price']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || userId != null) {
      map['user_id'] = Variable<String>(userId);
    }
    if (!nullToAbsent || categoryname != null) {
      map['categoryname'] = Variable<String>(categoryname);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    if (!nullToAbsent || price != null) {
      map['price'] = Variable<double>(price);
    }
    return map;
  }

  factory Item.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Item(
      id: serializer.fromJson<int>(json['id']),
      userId: serializer.fromJson<String>(json['userId']),
      categoryname: serializer.fromJson<String>(json['categoryname']),
      name: serializer.fromJson<String>(json['name']),
      price: serializer.fromJson<double>(json['price']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'userId': serializer.toJson<String>(userId),
      'categoryname': serializer.toJson<String>(categoryname),
      'name': serializer.toJson<String>(name),
      'price': serializer.toJson<double>(price),
    };
  }

  Item copyWith(
          {int id,
          String userId,
          String categoryname,
          String name,
          double price}) =>
      Item(
        id: id ?? this.id,
        userId: userId ?? this.userId,
        categoryname: categoryname ?? this.categoryname,
        name: name ?? this.name,
        price: price ?? this.price,
      );
  @override
  String toString() {
    return (StringBuffer('Item(')
          ..write('id: $id, ')
          ..write('userId: $userId, ')
          ..write('categoryname: $categoryname, ')
          ..write('name: $name, ')
          ..write('price: $price')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(userId.hashCode,
          $mrjc(categoryname.hashCode, $mrjc(name.hashCode, price.hashCode)))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Item &&
          other.id == this.id &&
          other.userId == this.userId &&
          other.categoryname == this.categoryname &&
          other.name == this.name &&
          other.price == this.price);
}

class ItemsCompanion extends UpdateCompanion<Item> {
  final Value<int> id;
  final Value<String> userId;
  final Value<String> categoryname;
  final Value<String> name;
  final Value<double> price;
  const ItemsCompanion({
    this.id = const Value.absent(),
    this.userId = const Value.absent(),
    this.categoryname = const Value.absent(),
    this.name = const Value.absent(),
    this.price = const Value.absent(),
  });
  ItemsCompanion.insert({
    this.id = const Value.absent(),
    @required String userId,
    this.categoryname = const Value.absent(),
    @required String name,
    @required double price,
  })  : userId = Value(userId),
        name = Value(name),
        price = Value(price);
  static Insertable<Item> custom({
    Expression<int> id,
    Expression<String> userId,
    Expression<String> categoryname,
    Expression<String> name,
    Expression<double> price,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (userId != null) 'user_id': userId,
      if (categoryname != null) 'categoryname': categoryname,
      if (name != null) 'name': name,
      if (price != null) 'price': price,
    });
  }

  ItemsCompanion copyWith(
      {Value<int> id,
      Value<String> userId,
      Value<String> categoryname,
      Value<String> name,
      Value<double> price}) {
    return ItemsCompanion(
      id: id ?? this.id,
      userId: userId ?? this.userId,
      categoryname: categoryname ?? this.categoryname,
      name: name ?? this.name,
      price: price ?? this.price,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (userId.present) {
      map['user_id'] = Variable<String>(userId.value);
    }
    if (categoryname.present) {
      map['categoryname'] = Variable<String>(categoryname.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (price.present) {
      map['price'] = Variable<double>(price.value);
    }
    return map;
  }
}

class $ItemsTable extends Items with TableInfo<$ItemsTable, Item> {
  final GeneratedDatabase _db;
  final String _alias;
  $ItemsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _userIdMeta = const VerificationMeta('userId');
  GeneratedTextColumn _userId;
  @override
  GeneratedTextColumn get userId => _userId ??= _constructUserId();
  GeneratedTextColumn _constructUserId() {
    return GeneratedTextColumn(
      'user_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _categorynameMeta =
      const VerificationMeta('categoryname');
  GeneratedTextColumn _categoryname;
  @override
  GeneratedTextColumn get categoryname =>
      _categoryname ??= _constructCategoryname();
  GeneratedTextColumn _constructCategoryname() {
    return GeneratedTextColumn('categoryname', $tableName, true,
        $customConstraints: 'NULL REFERENCES categories(name)');
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _priceMeta = const VerificationMeta('price');
  GeneratedRealColumn _price;
  @override
  GeneratedRealColumn get price => _price ??= _constructPrice();
  GeneratedRealColumn _constructPrice() {
    return GeneratedRealColumn(
      'price',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, userId, categoryname, name, price];
  @override
  $ItemsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'items';
  @override
  final String actualTableName = 'items';
  @override
  VerificationContext validateIntegrity(Insertable<Item> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('user_id')) {
      context.handle(_userIdMeta,
          userId.isAcceptableOrUnknown(data['user_id'], _userIdMeta));
    } else if (isInserting) {
      context.missing(_userIdMeta);
    }
    if (data.containsKey('categoryname')) {
      context.handle(
          _categorynameMeta,
          categoryname.isAcceptableOrUnknown(
              data['categoryname'], _categorynameMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name'], _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('price')) {
      context.handle(
          _priceMeta, price.isAcceptableOrUnknown(data['price'], _priceMeta));
    } else if (isInserting) {
      context.missing(_priceMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Item map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Item.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $ItemsTable createAlias(String alias) {
    return $ItemsTable(_db, alias);
  }
}

class Categorie extends DataClass implements Insertable<Categorie> {
  final int id;
  final String name;
  Categorie({@required this.id, @required this.name});
  factory Categorie.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return Categorie(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    return map;
  }

  factory Categorie.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Categorie(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
    };
  }

  Categorie copyWith({int id, String name}) => Categorie(
        id: id ?? this.id,
        name: name ?? this.name,
      );
  @override
  String toString() {
    return (StringBuffer('Categorie(')
          ..write('id: $id, ')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode, name.hashCode));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Categorie && other.id == this.id && other.name == this.name);
}

class CategoriesCompanion extends UpdateCompanion<Categorie> {
  final Value<int> id;
  final Value<String> name;
  const CategoriesCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
  });
  CategoriesCompanion.insert({
    this.id = const Value.absent(),
    @required String name,
  }) : name = Value(name);
  static Insertable<Categorie> custom({
    Expression<int> id,
    Expression<String> name,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
    });
  }

  CategoriesCompanion copyWith({Value<int> id, Value<String> name}) {
    return CategoriesCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    return map;
  }
}

class $CategoriesTable extends Categories
    with TableInfo<$CategoriesTable, Categorie> {
  final GeneratedDatabase _db;
  final String _alias;
  $CategoriesTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, name];
  @override
  $CategoriesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'categories';
  @override
  final String actualTableName = 'categories';
  @override
  VerificationContext validateIntegrity(Insertable<Categorie> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name'], _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Categorie map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Categorie.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $CategoriesTable createAlias(String alias) {
    return $CategoriesTable(_db, alias);
  }
}

class User extends DataClass implements Insertable<User> {
  final String phone;
  final String fullname;
  final String password;
  User(
      {@required this.phone, @required this.fullname, @required this.password});
  factory User.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    return User(
      phone:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}phone']),
      fullname: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}fullname']),
      password: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}password']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || phone != null) {
      map['phone'] = Variable<String>(phone);
    }
    if (!nullToAbsent || fullname != null) {
      map['fullname'] = Variable<String>(fullname);
    }
    if (!nullToAbsent || password != null) {
      map['password'] = Variable<String>(password);
    }
    return map;
  }

  factory User.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return User(
      phone: serializer.fromJson<String>(json['phone']),
      fullname: serializer.fromJson<String>(json['fullname']),
      password: serializer.fromJson<String>(json['password']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'phone': serializer.toJson<String>(phone),
      'fullname': serializer.toJson<String>(fullname),
      'password': serializer.toJson<String>(password),
    };
  }

  User copyWith({String phone, String fullname, String password}) => User(
        phone: phone ?? this.phone,
        fullname: fullname ?? this.fullname,
        password: password ?? this.password,
      );
  @override
  String toString() {
    return (StringBuffer('User(')
          ..write('phone: $phone, ')
          ..write('fullname: $fullname, ')
          ..write('password: $password')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(phone.hashCode, $mrjc(fullname.hashCode, password.hashCode)));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is User &&
          other.phone == this.phone &&
          other.fullname == this.fullname &&
          other.password == this.password);
}

class UsersCompanion extends UpdateCompanion<User> {
  final Value<String> phone;
  final Value<String> fullname;
  final Value<String> password;
  const UsersCompanion({
    this.phone = const Value.absent(),
    this.fullname = const Value.absent(),
    this.password = const Value.absent(),
  });
  UsersCompanion.insert({
    @required String phone,
    @required String fullname,
    @required String password,
  })  : phone = Value(phone),
        fullname = Value(fullname),
        password = Value(password);
  static Insertable<User> custom({
    Expression<String> phone,
    Expression<String> fullname,
    Expression<String> password,
  }) {
    return RawValuesInsertable({
      if (phone != null) 'phone': phone,
      if (fullname != null) 'fullname': fullname,
      if (password != null) 'password': password,
    });
  }

  UsersCompanion copyWith(
      {Value<String> phone, Value<String> fullname, Value<String> password}) {
    return UsersCompanion(
      phone: phone ?? this.phone,
      fullname: fullname ?? this.fullname,
      password: password ?? this.password,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (phone.present) {
      map['phone'] = Variable<String>(phone.value);
    }
    if (fullname.present) {
      map['fullname'] = Variable<String>(fullname.value);
    }
    if (password.present) {
      map['password'] = Variable<String>(password.value);
    }
    return map;
  }
}

class $UsersTable extends Users with TableInfo<$UsersTable, User> {
  final GeneratedDatabase _db;
  final String _alias;
  $UsersTable(this._db, [this._alias]);
  final VerificationMeta _phoneMeta = const VerificationMeta('phone');
  GeneratedTextColumn _phone;
  @override
  GeneratedTextColumn get phone => _phone ??= _constructPhone();
  GeneratedTextColumn _constructPhone() {
    return GeneratedTextColumn(
      'phone',
      $tableName,
      false,
    );
  }

  final VerificationMeta _fullnameMeta = const VerificationMeta('fullname');
  GeneratedTextColumn _fullname;
  @override
  GeneratedTextColumn get fullname => _fullname ??= _constructFullname();
  GeneratedTextColumn _constructFullname() {
    return GeneratedTextColumn(
      'fullname',
      $tableName,
      false,
    );
  }

  final VerificationMeta _passwordMeta = const VerificationMeta('password');
  GeneratedTextColumn _password;
  @override
  GeneratedTextColumn get password => _password ??= _constructPassword();
  GeneratedTextColumn _constructPassword() {
    return GeneratedTextColumn(
      'password',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [phone, fullname, password];
  @override
  $UsersTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'users';
  @override
  final String actualTableName = 'users';
  @override
  VerificationContext validateIntegrity(Insertable<User> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('phone')) {
      context.handle(
          _phoneMeta, phone.isAcceptableOrUnknown(data['phone'], _phoneMeta));
    } else if (isInserting) {
      context.missing(_phoneMeta);
    }
    if (data.containsKey('fullname')) {
      context.handle(_fullnameMeta,
          fullname.isAcceptableOrUnknown(data['fullname'], _fullnameMeta));
    } else if (isInserting) {
      context.missing(_fullnameMeta);
    }
    if (data.containsKey('password')) {
      context.handle(_passwordMeta,
          password.isAcceptableOrUnknown(data['password'], _passwordMeta));
    } else if (isInserting) {
      context.missing(_passwordMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {phone};
  @override
  User map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return User.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $UsersTable createAlias(String alias) {
    return $UsersTable(_db, alias);
  }
}

class UserSessionData extends DataClass implements Insertable<UserSessionData> {
  final bool logged;
  final String userId;
  UserSessionData({this.logged, @required this.userId});
  factory UserSessionData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final boolType = db.typeSystem.forDartType<bool>();
    final stringType = db.typeSystem.forDartType<String>();
    return UserSessionData(
      logged:
          boolType.mapFromDatabaseResponse(data['${effectivePrefix}logged']),
      userId:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}user_id']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || logged != null) {
      map['logged'] = Variable<bool>(logged);
    }
    if (!nullToAbsent || userId != null) {
      map['user_id'] = Variable<String>(userId);
    }
    return map;
  }

  factory UserSessionData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return UserSessionData(
      logged: serializer.fromJson<bool>(json['logged']),
      userId: serializer.fromJson<String>(json['userId']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'logged': serializer.toJson<bool>(logged),
      'userId': serializer.toJson<String>(userId),
    };
  }

  UserSessionData copyWith({bool logged, String userId}) => UserSessionData(
        logged: logged ?? this.logged,
        userId: userId ?? this.userId,
      );
  @override
  String toString() {
    return (StringBuffer('UserSessionData(')
          ..write('logged: $logged, ')
          ..write('userId: $userId')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(logged.hashCode, userId.hashCode));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is UserSessionData &&
          other.logged == this.logged &&
          other.userId == this.userId);
}

class UserSessionCompanion extends UpdateCompanion<UserSessionData> {
  final Value<bool> logged;
  final Value<String> userId;
  const UserSessionCompanion({
    this.logged = const Value.absent(),
    this.userId = const Value.absent(),
  });
  UserSessionCompanion.insert({
    this.logged = const Value.absent(),
    @required String userId,
  }) : userId = Value(userId);
  static Insertable<UserSessionData> custom({
    Expression<bool> logged,
    Expression<String> userId,
  }) {
    return RawValuesInsertable({
      if (logged != null) 'logged': logged,
      if (userId != null) 'user_id': userId,
    });
  }

  UserSessionCompanion copyWith({Value<bool> logged, Value<String> userId}) {
    return UserSessionCompanion(
      logged: logged ?? this.logged,
      userId: userId ?? this.userId,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (logged.present) {
      map['logged'] = Variable<bool>(logged.value);
    }
    if (userId.present) {
      map['user_id'] = Variable<String>(userId.value);
    }
    return map;
  }
}

class $UserSessionTable extends UserSession
    with TableInfo<$UserSessionTable, UserSessionData> {
  final GeneratedDatabase _db;
  final String _alias;
  $UserSessionTable(this._db, [this._alias]);
  final VerificationMeta _loggedMeta = const VerificationMeta('logged');
  GeneratedBoolColumn _logged;
  @override
  GeneratedBoolColumn get logged => _logged ??= _constructLogged();
  GeneratedBoolColumn _constructLogged() {
    return GeneratedBoolColumn(
      'logged',
      $tableName,
      true,
    );
  }

  final VerificationMeta _userIdMeta = const VerificationMeta('userId');
  GeneratedTextColumn _userId;
  @override
  GeneratedTextColumn get userId => _userId ??= _constructUserId();
  GeneratedTextColumn _constructUserId() {
    return GeneratedTextColumn(
      'user_id',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [logged, userId];
  @override
  $UserSessionTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'user_session';
  @override
  final String actualTableName = 'user_session';
  @override
  VerificationContext validateIntegrity(Insertable<UserSessionData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('logged')) {
      context.handle(_loggedMeta,
          logged.isAcceptableOrUnknown(data['logged'], _loggedMeta));
    }
    if (data.containsKey('user_id')) {
      context.handle(_userIdMeta,
          userId.isAcceptableOrUnknown(data['user_id'], _userIdMeta));
    } else if (isInserting) {
      context.missing(_userIdMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {userId};
  @override
  UserSessionData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return UserSessionData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $UserSessionTable createAlias(String alias) {
    return $UserSessionTable(_db, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $ExpenseTable _expense;
  $ExpenseTable get expense => _expense ??= $ExpenseTable(this);
  $IncomeTable _income;
  $IncomeTable get income => _income ??= $IncomeTable(this);
  $BalanceTable _balance;
  $BalanceTable get balance => _balance ??= $BalanceTable(this);
  $ItemsTable _items;
  $ItemsTable get items => _items ??= $ItemsTable(this);
  $CategoriesTable _categories;
  $CategoriesTable get categories => _categories ??= $CategoriesTable(this);
  $UsersTable _users;
  $UsersTable get users => _users ??= $UsersTable(this);
  $UserSessionTable _userSession;
  $UserSessionTable get userSession => _userSession ??= $UserSessionTable(this);
  IncomeDao _incomeDao;
  IncomeDao get incomeDao => _incomeDao ??= IncomeDao(this as AppDatabase);
  ExpenseDao _expenseDao;
  ExpenseDao get expenseDao => _expenseDao ??= ExpenseDao(this as AppDatabase);
  ItemDao _itemDao;
  ItemDao get itemDao => _itemDao ??= ItemDao(this as AppDatabase);
  CategoriesDao _categoriesDao;
  CategoriesDao get categoriesDao =>
      _categoriesDao ??= CategoriesDao(this as AppDatabase);
  UserDao _userDao;
  UserDao get userDao => _userDao ??= UserDao(this as AppDatabase);
  SessionDao _sessionDao;
  SessionDao get sessionDao => _sessionDao ??= SessionDao(this as AppDatabase);
  BalanceDao _balanceDao;
  BalanceDao get balanceDao => _balanceDao ??= BalanceDao(this as AppDatabase);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities =>
      [expense, income, balance, items, categories, users, userSession];
}

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$UserDaoMixin on DatabaseAccessor<AppDatabase> {
  $UsersTable get users => attachedDatabase.users;
}
mixin _$SessionDaoMixin on DatabaseAccessor<AppDatabase> {
  $UserSessionTable get userSession => attachedDatabase.userSession;
}
mixin _$BalanceDaoMixin on DatabaseAccessor<AppDatabase> {
  $BalanceTable get balance => attachedDatabase.balance;
}
mixin _$IncomeDaoMixin on DatabaseAccessor<AppDatabase> {
  $IncomeTable get income => attachedDatabase.income;
}
mixin _$CategoriesDaoMixin on DatabaseAccessor<AppDatabase> {
  $CategoriesTable get categories => attachedDatabase.categories;
}
mixin _$ExpenseDaoMixin on DatabaseAccessor<AppDatabase> {
  $ExpenseTable get expense => attachedDatabase.expense;
}
mixin _$ItemDaoMixin on DatabaseAccessor<AppDatabase> {
  $ItemsTable get items => attachedDatabase.items;
}
