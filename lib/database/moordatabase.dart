import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'moordatabase.g.dart';

//Table for Expense
class Expense extends Table{

IntColumn get id=> integer().autoIncrement()();
RealColumn get totalExpense=> real()();
TextColumn get userId=> text()();
TextColumn get description=>text().nullable()();
DateTimeColumn get expenseDate=> dateTime().nullable()();

}

//Table for Income
class Income extends Table{

IntColumn get id=> integer().autoIncrement()();
TextColumn get userId=> text()();
RealColumn get totalIncome=> real()();
DateTimeColumn get incomeDate=> dateTime().nullable()();

}
//Table for Balance
class Balance extends Table{
TextColumn get userId=> text()();
RealColumn get totalBalance=> real()();
DateTimeColumn get balanceDate=> dateTime().nullable()();
@override
  Set<Column> get primaryKey=> {userId};

}

//Table for Items
class Items extends Table{
  IntColumn get id=> integer().autoIncrement()();
  TextColumn get userId=> text()();
  TextColumn get categoryname=> text().nullable().customConstraint('NULL REFERENCES categories(name)')(); //Foreign key from categories
  TextColumn get name=> text()();
  RealColumn get price=>real()();
}

//Table for Categories
class Categories extends Table{
  IntColumn get id=> integer().autoIncrement()();
  TextColumn get name=>text()();
}

//Storing User Session
class UserSession extends Table{
  BoolColumn get logged => boolean().nullable()();
  TextColumn get userId => text()();
   @override
  Set<Column> get primaryKey=> {userId};
}

//Table for User
class Users extends Table{
  TextColumn get phone=> text()();
  TextColumn get fullname=>text()();
  TextColumn get password=> text()();
  @override
  Set<Column> get primaryKey=> {phone};
}

@UseMoor(tables:[Expense,Income,Balance,Items,Categories,Users,UserSession],
daos: [IncomeDao,ExpenseDao,ItemDao,CategoriesDao,UserDao,SessionDao,BalanceDao])

class AppDatabase extends _$AppDatabase{

AppDatabase():super(FlutterQueryExecutor.inDatabaseFolder(path: 'db.sqlite'));

  @override
  int get schemaVersion => 1;

}


//Users Operation
@UseDao(tables: [Users])
class UserDao extends DatabaseAccessor<AppDatabase> with _$UserDaoMixin {
  UserDao(AppDatabase db) : super(db);

  Future addUser(UsersCompanion user)=>into(users).insert(user);
  Future<List<User>> getAllUsers()=> select(users).get();

  Future<List<User>> getUser(id){
    return (select(users)..where((tbl) => tbl.phone.equals(id))).get();
  }

  Future<bool> login(user,pass)async{
  return getAllUsers().then((val){
    if(val.where((val)=>val.phone ==user && val.password==pass).length>0){
      final userid = UserSessionCompanion(userId: Value(user),logged:Value(true));
       db.sessionDao.setSession(userid);
      // await SessionDao(db).setSession(userid);  //For Setting Session ID 
      print('Session Initiated');
      return true;
    }
    
    
    return false;
    });
  }

  Future<bool> logout(id){
    return getUser(id).then((val)async{
        if(val.where((element) => element.phone==id).isNotEmpty){
        final sid = UserSessionCompanion(userId: Value(id));
       db.sessionDao.disposeSession(sid).then((value){
        print(value);
      });
      print('Session Destroyed');
      return true;
      }
      
      return false;
    });
  }
  
  Future<bool> checkUser(user)async{
  return getAllUsers().then((val){
    if(val.where((val)=>val.phone==user).length==0){
      return false;
    }
    return true;
    });
  }
}  

//Dao for user sessions
@UseDao(tables: [UserSession])
class SessionDao extends DatabaseAccessor<AppDatabase> with _$SessionDaoMixin{
  SessionDao(AppDatabase db) : super(db);

  Future<List<UserSessionData>> getSessionId(){
    return select(userSession).get();
  }
  Future setSession(Insertable<UserSessionData> sessionData)=> into(userSession).insert(sessionData);
  Future disposeSession(Insertable<UserSessionData> dispose)=>delete(userSession).delete(dispose);
}

@UseDao(tables:[Balance])
class BalanceDao extends DatabaseAccessor<AppDatabase> with _$BalanceDaoMixin{
  BalanceDao(AppDatabase attachedDatabase) : super(attachedDatabase);

  Stream<BalanceData> getbalance(id){
    return select(balance).watchSingle();
  }
  // Stream<BalanceData> totalBalance(int id){
  //   return (select(balance)..).watchSingle();
  // }

  Future addBalance(BalanceCompanion balanceData)=> into(balance).insert(balanceData); //add income to database

  Future updateBalance(BalanceCompanion data) => update(balance).replace(data);
}

//Operations for Income
@UseDao(tables: [Income])
class IncomeDao extends DatabaseAccessor<AppDatabase> with _$IncomeDaoMixin {
  IncomeDao(AppDatabase db) : super(db);

  //For Income
  Future<List<IncomeData>> getAllIncome(id)=> (select(income)..orderBy([
(u)=>OrderingTerm(expression: u.userId.equals(id))
  ])).get(); // get all data related to income

  Stream<List<IncomeData>> getIncomeList(id)=> (select(income)..where((tbl) => tbl.userId.equals(id))).watch();

  Stream<List<IncomeData>> watchAllIncome(){ // For Sorting out and fetching income data
   return( select(income)
   ..orderBy([
     (t)=> OrderingTerm(expression: t.incomeDate, mode: OrderingMode.desc)

   ])).watch();
  }
  Future addIncome(IncomeCompanion incomeData)=> into(income).insert(incomeData); //add income to database
  Future updateIncome(IncomeCompanion incomeData)=> update(income).replace(incomeData); //update income
  Future deleteIncome(IncomeCompanion incomeData)=> delete(income).delete(incomeData);  //delete


}

//Operations for Categories
@UseDao(tables: [Categories])
class CategoriesDao extends DatabaseAccessor<AppDatabase> with _$CategoriesDaoMixin {
  CategoriesDao(AppDatabase db) : super(db);

  Future<List<Categorie>> getAllCategory()=>select(categories).get();

  Future initialCategory(List<String> cat) async{
   cat.forEach((element) async{ 
     await batch((batch){
       batch.insertAll(categories, [
         CategoriesCompanion.insert(name: element)
       ]);
     });
   });
  }

  Future addCategory(Insertable<Categorie> categorie)=> into(categories).insert(categorie);

}

//Operations for Expenses
@UseDao(tables:[Expense])
class ExpenseDao extends DatabaseAccessor<AppDatabase> with _$ExpenseDaoMixin {
  ExpenseDao(AppDatabase db) : super(db);

  //For Expense
 Future<List<ExpenseData>> getAllExpense(id)=> (select(expense)..orderBy([
(u)=>OrderingTerm(expression: u.userId.equals(id))
  ])).get(); //get all data related to expense

   Stream<List<ExpenseData>> getExpenseList(id)=> (select(expense)..where((tbl) => tbl.userId.equals(id))).watch();

  //Ordering of generated list of data by: amount or date
  

   Future<List<ExpenseData>> orderbydate(id,DateTime dateTime){
    return (select(expense)
    ..orderBy([
      (t)=>OrderingTerm(expression: t.userId.equals(id)),
      (u)=> OrderingTerm(expression: u.expenseDate.equals(dateTime))
    ])).get();
  }

  Future addExpense(ExpenseCompanion expenseData)=> into(expense).insert(expenseData); //add expense to database
  Future deleteExpense(ExpenseCompanion expenseData)=> delete(expense).delete(expenseData);  //delete
}


//Operations for Items
@UseDao(tables: [Items])
class ItemDao extends DatabaseAccessor<AppDatabase> with _$ItemDaoMixin{
  ItemDao(AppDatabase db) : super(db);


  
  //List All Items
  Stream<List<Item>> getAllItems(balance)=>(select(items)..where((tbl) => tbl.price.isSmallerOrEqualValue(balance))).watch();
  
  
  //Adding and Deleting
  Future addItems(ItemsCompanion item)=>into(items).insert(item);
  Future deleteItems(ItemsCompanion item)=>delete(items).delete(item);
  Future initialItems(id)async{

      await batch((batch){
        batch.insertAll(items, [
          ItemsCompanion.insert(userId: id, name: 'Death; An Inside Story', price: 500,categoryname: Value('Books')),
          ItemsCompanion.insert(userId: id, name: 'You Can Win', price: 450,categoryname: Value('Books')),
          ItemsCompanion.insert(userId: id, name: 'The Great Gatsby', price: 300,categoryname: Value('Books')),
          ItemsCompanion.insert(userId: id, name: 'Realme 5 pro', price: 20000,categoryname: Value('Electronics')),
          ItemsCompanion.insert(userId: id, name: 'Sony Earphone', price: 500,categoryname: Value('Electronics')),
          ItemsCompanion.insert(userId: id, name: 'Portable Speaker', price: 480,categoryname: Value('Electronics')),
          ItemsCompanion.insert(userId: id, name: 'Pulsar 150cc', price: 180000,categoryname: Value('Bikes')),
          ItemsCompanion.insert(userId: id, name: 'Activa 2020', price: 150000,categoryname: Value('Bikes')),
          ItemsCompanion.insert(userId: id, name: 'Honda', price: 200000,categoryname: Value('Bikes')),
          ItemsCompanion.insert(userId: id, name: 'Tshirt', price: 650,categoryname: Value('Clothes')),
          ItemsCompanion.insert(userId: id, name: 'Pant', price: 1200,categoryname: Value('Clothes')),
          ItemsCompanion.insert(userId: id, name: 'Shoes', price: 1500,categoryname: Value('Clothes')),
        ]);
      });
    
  }
}